import { Injectable } from '@nestjs/common';
import {
    ErrorResultUnion,
    ID,
    Order,
    RequestContext,
    TransactionalConnection
} from '@vendure/core';
import { UpdateOrderItemsResult } from '@vendure/common/lib/generated-shop-types';
import { DoSomethingToOrderResult } from '../generated-shop-types';
import { OrderModificationError } from '@vendure/core/dist/common/error/generated-graphql-shop-errors';

@Injectable()
export class ExampleService {
    constructor(
        private connection: TransactionalConnection,
    ) {}

    async doSomethingToOrder(
        ctx: RequestContext,
        orderId: ID,
    // ): Promise<ErrorResultUnion<UpdateOrderItemsResult, Order>> {
    ): Promise<ErrorResultUnion<DoSomethingToOrderResult, Order>> {
        return new OrderModificationError();
        
    }
}
