import { Args, Mutation, ResolveField, Resolver } from '@nestjs/graphql';
import { Allow, Ctx, ErrorResultUnion, Permission, RequestContext, Transaction } from '@vendure/core';
import { ActiveOrderService, Order } from '@vendure/core';
import { UpdateOrderItemsResult } from '@vendure/common/lib/generated-shop-types';
import { ExampleService } from '../service/example.service';
import { DoSomethingToOrderResult, MutationdoSomethingToOrderArgs } from '../generated-shop-types';

@Resolver()
export class OrderResolver {

    constructor(
        private exampleService: ExampleService,
        private activeOrderService: ActiveOrderService
    ) {}

    @Transaction()
    @Mutation()
    @Allow(Permission.UpdateOrder, Permission.Owner)
    async doSomethingToOrder(
        @Ctx() ctx: RequestContext,
        @Args() args: MutationdoSomethingToOrderArgs,
    // ): Promise<ErrorResultUnion<UpdateOrderItemsResult, Order>> {
    ): Promise<ErrorResultUnion<DoSomethingToOrderResult, Order>> {
        const order = await this.activeOrderService.getOrderFromContext(ctx, true);
        return this.exampleService.doSomethingToOrder(
            ctx,
            order.id
        );
    }

}

@Resolver('DoSomethingToOrderResult')
export class DoSomethingToOrderResultResolver {

  @ResolveField()
  __resolveType(value: any): string {
    // If it has an "id" property we can assume it is an Order.
    if (value.hasOwnProperty('id')) {
        return 'Order';
    } else if (value.message == "ORDER_MODIFICATION_ERROR") {
        return 'OrderModificationError';
    } else if (value.message == "ORDER_LIMIT_ERROR") {
        return 'OrderLimitError';
    } else if (value.message == "NEGATIVE_QUANTITY_ERROR") {
        return 'NegativeQuantityError';
    } else if (value.message = "INSUFFICIENT_STOCK_ERROR") {
        return 'InsufficientStockError';
    } else {
        return 'CustomExampleError';
    }
  }
}