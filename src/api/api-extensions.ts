import { gql } from 'apollo-server-core';

export const commonApiExtensions = gql`
    type CustomExampleError implements ErrorResult {
        errorCode: ErrorCode!
        message: String!
    }
`;

export const shopApiExtensions = gql`
    ${commonApiExtensions}

    extend type Mutation {
        doSomethingToOrder(orderId: ID!): DoSomethingToOrderResult!
    }

    union DoSomethingToOrderResult = Order | OrderModificationError | OrderLimitError | NegativeQuantityError | InsufficientStockError | CustomExampleError
`;

export const adminApiExtensions = gql`
    ${commonApiExtensions}
`;
