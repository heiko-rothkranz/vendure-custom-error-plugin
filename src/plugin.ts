import { PluginCommonModule, VendurePlugin } from '@vendure/core';

import { adminApiExtensions, shopApiExtensions } from './api/api-extensions';
import { ExampleService } from './service/example.service';
import { OrderResolver, DoSomethingToOrderResultResolver } from './api/order.resolver';

@VendurePlugin({
    imports: [PluginCommonModule],
    entities: [],
    adminApiExtensions: {
        schema: adminApiExtensions,
        resolvers: [],
    },
    shopApiExtensions: {
        schema: shopApiExtensions,
        resolvers: [
            OrderResolver,
            DoSomethingToOrderResultResolver,
        ],
    },
    providers: [
        ExampleService,
        // By definiting the `PLUGIN_INIT_OPTIONS` symbol as a provider, we can then inject the
        // user-defined options into other classes, such as the {@link ExampleService}.
        // { provide: PLUGIN_INIT_OPTIONS, useFactory: () => ExamplePlugin.options },
    ],
})
export class CustomErrorPlugin {}
