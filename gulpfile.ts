import { dest, parallel, src, watch as gulpWatch } from 'gulp';

const TRANSLATION_FILES = ['**/i18n/**/*'];

function copyTranslationsSource() {
    return src(TRANSLATION_FILES, { cwd: 'src' }).pipe(dest('lib/src'));
}
export const build = parallel(copyTranslationsSource);

function copyToDist(filePath: string) {
    console.log('copyToDist', filePath);
    src(filePath, { cwd: 'src' }).pipe(dest('lib/src/i18n'));
}

export function watch() {
    const watcher = gulpWatch(TRANSLATION_FILES, { cwd: 'src' });
    watcher.on('change', copyToDist);
    watcher.on('add', copyToDist);
    return new Promise(() => {});
}